What's this?
============

This is a repository for the filesystem I overlay whenever I flash a firmware update on my ASUS RT-N66U.

What did you change?
====================

Note: this list is probably outdated since I forget this file exists

    - Custom config files ((https://github.com/RMerl/asuswrt-merlin/wiki/Custom-config-files)[link]).
        - `dnsmasq.conf`: (appended)
            - Enable `ra-names` for DHCPv6 (requires disabling RA first)
            - `domain-needed` to avoid sending non-dotted requests upstream
            - `bogus-priv` disables forwarding local-only rDNS requests
            - Added specific ntp-server for DHCPv6 .. unfortunately the DHCPv4 ntp-server option only supports IP's

What configuration did you change in the webinterface?
======================================================

Some of the options I set implicitly require some other settings in the webinterface:

    - LAN -> DHCP Server
        - Router's Domain Name
        - Forward local domain queries to upstream DNS
            - Otherwise you won't get externally configured records for anything matching your domain name
        - Enable DNSSEC support
            - Not required per se, but is highly recommended if you're just the slighest security concious. It does however impact performance in the sense that most DNS requests would have to be retried in TCP mode alone because the DNSSEC signature(s) are really large.

more tba




